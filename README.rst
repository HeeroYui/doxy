Lutin
=====

`lutin` is a generic builder and package maker is a FREE software tool.


.. image:: https://badge.fury.io/py/lutin.png
    :target: https://pypi.python.org/pypi/lutin

Release (master)
----------------

.. image:: https://travis-ci.org/HeeroYui/lutin.svg?branch=master
    :target: https://travis-ci.org/HeeroYui/lutin


.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=master&tag=Linux
    :target: http://atria-soft.com/ci/HeeroYui/lutin
.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=master&tag=MacOs
    :target: http://atria-soft.com/ci/HeeroYui/lutin
.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=master&tag=Mingw
    :target: http://atria-soft.com/ci/HeeroYui/lutin


.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=master&tag=Android
    :target: http://atria-soft.com/ci/HeeroYui/lutin
.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=master&tag=IOs
    :target: http://atria-soft.com/ci/HeeroYui/lutin


Developement (dev)
------------------

.. image:: https://travis-ci.org/HeeroYui/lutin.svg?branch=dev
    :target: https://travis-ci.org/HeeroYui/lutin


.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=dev&tag=Linux
    :target: http://atria-soft.com/ci/HeeroYui/lutin
.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=dev&tag=MacOs
    :target: http://atria-soft.com/ci/HeeroYui/lutin
.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=dev&tag=Mingw
    :target: http://atria-soft.com/ci/HeeroYui/lutin


.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=dev&tag=Android
    :target: http://atria-soft.com/ci/HeeroYui/lutin
.. image:: http://atria-soft.com/ci/build/HeeroYui/lutin.svg?branch=dev&tag=IOs
    :target: http://atria-soft.com/ci/HeeroYui/lutin


Instructions
------------

This is a tool to generate the binary, shared library, static library and package independently of the OS

This software builds C, C++, m, m++, to object file and generate associated libraries (.so & .a) end create final Executable file

This tool can generate package for Linux, MacOs, Android


Lutin is under a FREE license that can be found in the COPYING file.
Any contribution is more than welcome ;)

git repository
--------------

http://github.com/HeeroYui/lutin/

Documentation
-------------

http://github.io/HeeroYui/lutin/

Installation
------------

Requirements: ``Python >= 2.7`` and ``pip``

Just run:

  pip install lutin

Install pip on debian/ubuntu:

  sudo apt-get install pip

Install pip on ARCH-linux:

  sudo pacman -S pip

Install pip on MacOs:

  sudo easy_install pip


License (APACHE v2.0)
---------------------

Copyright lutin Edouard DUPIN

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

